const fs = require('fs')
const path = require('path')
const express = require('express')
const app = express()

const MarkdownIt = require( 'markdown-it' ),
    md = new MarkdownIt();
// console.log( markdown.toHTML( "Hello *World*!" ) );

const ruta= path.resolve(__dirname)+'/archivos';
var cors = require('cors');

app.use(cors());
app.use(express.json())
app.listen(3001, () => {
    console.log("Escuchando en: http://localhost:3001")
});

app.get('/', (request, response) => {
    response.sendFile(path.resolve(__dirname, 'files.html'))
})
app.get('/listFiles', (request, response) => {
    // const ruta = path.resolve(__dirname);
    fs.readdir(ruta, (err, files) => {
        response.json({
            files: files
        })
        // files.forEach(file => {
        //   console.log(file);
        // });
      });
})
app.post('/createFiles', (request, response) => {
    console.log("crateFiles******");
    console.log(request.body.name);

    const name=`${ruta}/${request.body.name}.md`;
    const content=request.body.content;
    // var result = md.render('# markdown-it rulezz!');
     console.log(name);
    fs.writeFile(name, content, function (err) {
        if (err) throw err;
        console.log('Saved!');
        response.status(200).json({
            success:'creado'
        })
    });
})
app.post('/getFile', (request, response) => {
    

    const name=`${ruta}/${request.body.name}`;
    // const content=request.body.content;
    // var result = md.render('# markdown-it rulezz!');

    fs.readFile(name, 'utf8',function(err, data) {
        const result = md.render(data);
       

        // response.writeHead(200, {'Content-Type': 'text/html'});
        // response.write(md.render(data));
        // return response.end();
        response.json({
            // text: md.render(data)
            text: result
        })
    });
})
app.get('/recitar', (request, response) => {
    fs.readFile(path.resolve(__dirname, 'priv/poema.txt'), 'utf8',
        (err, data) => {
            if (err) {
                console.error(err)
                response.status(500).json({
                    error: 'message'
                })
                return
            }
            response.json({
                text: data.replace(/\n/g, '<br>')
            })
        })
    //
})

app.post('/', (request, response) => {
    console.log("post");
    console.log(request.body)
    // let markDownText = request.body.text
    // console.log(markDownText)
    // let htmlText = md.render(markDownText)
    response.setHeader('Content-Type', 'application/json')
    response.end(JSON.stringify({
        text: "htmlText"
    }))
})
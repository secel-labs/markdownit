Veo lo que creo ver y no veo más
De lo que pueda contar no recuerdo nada
No hay necesidad de cantarlo más
Creo creer, temo temer que esto es verdad
La vida se nubló en su totalidad
Estoy perdido
Y no sé mirar lo que dejé allá atrás
En ese camino largo que un día me vio caminar
Nació esta ciega herida
Que borró hoy día mi ayer
Personas extrañas hablan de quien fui
Pretenden darme calor sin que sepa nada
No hay necesidad ni siquiera de llorar por estar así
Mi amnesia me dice absolutamente nada más
Que esta sensación de ansiedad
En ese camino largo que un día me vio caminar
Quemé una biografía
Y soplé cenizas del ayer
No intenten enseñarme
Quien me quiso y a quien debo amar
Comienza el día cero
Y mañana su continuidad
Sha da da da daun
Sha da da da daun
Y soplé cenizas del ayer
Sha da da da daun
Sha da da da daun
Y soplé cenizas de mi ayer
No intenten enseñarme
Sha da da da daun
Sha da da da daun
Quien me quiso y a quien debo amar
Sha da da da daun
Sha da da da daun
Sha da da da daun
Sha da da da daun
Sha da da da daun
Sha da da da daun
Quien me quiso y a quien debo amar